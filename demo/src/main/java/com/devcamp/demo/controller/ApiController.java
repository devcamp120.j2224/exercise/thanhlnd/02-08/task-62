package com.devcamp.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.demo.model.District;
import com.devcamp.demo.model.Province;
import com.devcamp.demo.model.Ward;
import com.devcamp.demo.repository.IDistrictRepository;
import com.devcamp.demo.repository.IProvinceRepository;
import com.devcamp.demo.repository.IWardRepository;

@RestController
@CrossOrigin
public class ApiController {
    
    @Autowired
    IProvinceRepository iProvinceRes;

    @Autowired
    IDistrictRepository iDistrictRes;

    @Autowired
    IWardRepository iWardRes;
    

    @GetMapping("province-info")
    public ResponseEntity<Set<District>> getDisStrictList(@RequestParam(name = "districtId") int districtId) {
        try {
            Province province =  iProvinceRes.findById(districtId);
            if(province!=null){
                return new ResponseEntity<>(province.getDistrict(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("province")
    public ResponseEntity<List<Province>> getProvinceList(){
        try {
            List<Province> provinceList = new ArrayList<>();
            iProvinceRes.findAll().forEach(provinceList::add);
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("district-info")
    public ResponseEntity<Set <Ward>> getWardList(@RequestParam(name = "id") int wardId) {

        
        //return new ResponseEntity<>(iDistrictRes.findById(wardId) , HttpStatus.OK);
        try {
            District district =  iDistrictRes.findById(wardId);
            if(district != null) {
                return new ResponseEntity<>(district.getWard(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }
}
