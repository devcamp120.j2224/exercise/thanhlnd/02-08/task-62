package com.devcamp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.demo.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Long>{
    
}
