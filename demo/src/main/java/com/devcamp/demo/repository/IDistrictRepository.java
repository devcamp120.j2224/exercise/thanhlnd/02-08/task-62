package com.devcamp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.demo.model.District;

public interface IDistrictRepository extends JpaRepository<District,Long>{
    District findById(int wardId);
}
